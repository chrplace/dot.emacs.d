;; Fix for bug 34341 
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

(require 'package)
(setq package-archives '(("org"   . "http://orgmode.org/elpa/")
			 ("gnu"   . "http://elpa.gnu.org/packages/")
			 ("melpa" . "https://melpa.org/packages/")))
(setq package-archive-priorities
      '(("org" . 20)
        ("melpa" . 10)
        ("gnu" . 0)))
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))
(package-install 'use-package)

(require 'use-package)

(use-package org
  :ensure t
  :pin gnu)
(org-babel-load-file (expand-file-name "~/.emacs.d/emacs-init.org"))
